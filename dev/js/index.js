//Slider principal
import { tns } from "./modules/tiny-slider"

//WOW JS
import { WOW } from 'wowjs/dist/wow.min'
//Headroom JS
import Headroom from 'headroom.js/dist/headroom.min'
//BaguetteBox.js
import BaguetteBox from 'baguettebox.js/dist/baguetteBox.min'

//Data vuejs
import {contactform} from './modules/contactform'
import { menuinicio, mainmenu} from './modules/menus'
import { proveedores } from './modules/proveedores'

// import Vue from 'vue/dist/vue.min'
import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'

Vue.use(VueResource);

new Vue({
  el: '#app',
  data: {
    showmodal: false,
    toggle: false,
    formSubmitted: false,
    vue: contactform,
    menuinicio,
    mainmenu,
    proveedores
  },
  created:() => {
    let wow = new WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: false,
      live: false
    })
    wow.init()
    //FOOTER AÑO
    function getDate() {
      var today = new Date()
      var year = today.getFullYear()
      document.getElementById('currentDate').innerHTML = year
    }
    getDate()
  },
  mounted:() => {
    var header = new Headroom(document.querySelector('#header'), {
      tolerance: 15,
      offset: 180,
      tolerance: {
        up: 5,
        down: 0
      },
      classes: {
        initial: 'header-initial', //Cuando el elemento se inicializa
        pinned: 'header-up', //Cuando se deplaza hacia arriba
        unpinned: 'header-down', //Cuando se deplaza hacia abajo
        top: 'header-top', //Cuando esta pegado arriba
        notTop: 'header-notop', //Cuando no esta pegado arriba
        bottom: 'header-bottom', //Cuando esta pegado abajo
        notBottom: 'header-nobottom'//Cuando no esta pegado abajo
      }
    })
    header.init()

    BaguetteBox.run('.card', {
      // Custom options
    });

    let slider = tns({
      container: '.my-slider',
      controlsText: [
        '<',
        '>'
      ],
      autoplayText: ['▶', '❚❚'],
      autoplay: true,
      speed: 2000,
      lazyload: true
    })
    let carousel = tns({
      container: '.carousel',
      controls: false,
      responsive: {
        "550": {
          "items": 4
        },
        "840": {
          "items": 8
        },
        "1024": {
          "items": 10
        }
      },
      autoplayDirection: 'backward',
      autoplayText: ['▶', '❚❚'],
      autoplay: true,
      autoplayTimeout: 1500,
      speed: 1000,
      lazyload: true
    })
  },
  methods: {
    isFormValid: function () {
      return this.nombre != ''
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('/mail.php', { vue: this.vue }).then(function (response) {
        // console.log(response)
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    toggleMenu: function () {
      if (this.toggle == true) {
        this.toggle = false
        console.log('DESACTIVADO')
      }
      else {
        this.toggle = true
        console.log('ACTIVO')
      }
    }    
  }
})