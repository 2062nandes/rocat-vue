export const proveedores = [
  {
    src: '/images/COOPER.png',
    title: 'COOPER'
  },
  {
    src: '/images/FALK.png',
    title: 'FALK'
  },
  {
    src: '/images/FSQ.jpg',
    title: 'FSQ'
  },
  {
    src: '/images/INA-FAG.jpg',
    title: 'INA-FAG'
  },
  {
    src: '/images/MARTIN.jpg',
    title: 'MARTIN'
  },
  {
    src: '/images/NSK.bmp',
    title: 'NSK'
  },
  {
    src: '/images/SKF.jpg',
    title: 'SKF'
  },
  {
    src: '/images/CyUB.png',
    title: 'CyUB'
  },
  {
    src: '/images/FKG.jpg',
    title: 'FKG'
  },
  {
    src: '/images/GATES.jpg',
    title: 'GATES'
  },
  {
    src: '/images/Koyo.jpg',
    title: 'Koyo'
  },
  {
    src: '/images/McGill.jpg',
    title: 'McGill'
  },
  {
    src: '/images/NTN.gif',
    title: 'NTN'
  },
  {
    src: '/images/ZWZ.jpg',
    title: 'ZWZ'
  }
]