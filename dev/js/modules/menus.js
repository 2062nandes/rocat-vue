export const menuinicio =[
    {
      href: '/',
      title: 'Inicio',
      delay: '0.8s'
    },
    {
      href: '/nosotros',
      title: 'Nosotros',
      delay: '0.9s'
    },
    {
      href: '/contactos',
      title: 'Contactos',
      delay: '1.0s'
    }
  ]
export const mainmenu = [
  {
    href: '/rodamientos',
    title: 'Rodamientos',
    slide: '/images/slide1.jpg',
    delay: '1.20s'
  },
  {
    href: '/retenes',
    title: 'Retenes',
    slide: '/images/slide2.jpg',
    delay: '1.05s'
  },
  {
    href: '/chumaceras',
    title: 'Chumaceras',
    slide: '/images/slide3.jpg',
    delay: '0.90s'
  },
  {
    href: '/acoples',
    title: 'Acoples',
    slide: '/images/slide4.jpg',
    delay: '0.75s'
  },
  {
    href: '/correas',
    title: 'Correas',
    slide: '/images/slide5.jpg',
    delay: '0.60s'
  },
  {
    href: '/cadenas',
    title: 'Cadenas',
    slide: '/images/slide6.jpg',
    delay: '0.45s'
  },
  {
    href: '/pinones',
    title: 'Piñones',
    slide: '/images/slide7.jpg',
    delay: '0.30s'
  },
  {
    href: '/herramientas',
    title: 'Herramientas',
    slide: '/images/slide8.jpg',
    delay: '0.15s'
  },
  {
    href: '/capacitacion',
    title: 'Capacitación',
    slide: '/images/slide9.jpg',
    delay: '0.00s',
    cursos: [
      {
        href: '/#alineacion-de-ejes',
        title: 'Alineación de ejes'
      },
      {
        href: '/#introduccion-a-los-rodamientos',
        title: 'Introducción a los rodamientos'
      },
      {
        href: '/#montaje-y-desmontaje',
        title: 'Montaje y desmontaje'
      },
      {
        href: '/#ajustes-y-tolerancias',
        title: 'Ajustes y tolerancias'
      },
      {
        href: '/#lubricacion',
        title: 'Lubricación'
      },
      {
        href: '/#transmision-de-potencia',
        title: 'Transmisión de potencia'
      },
      {
        href: '/#fallas-de-rodamiento',
        title: 'Fallas de rodamiento'
      },
      {
        href: '/#termografia',
        title: 'Termografía'
      }
    ]
  }
];